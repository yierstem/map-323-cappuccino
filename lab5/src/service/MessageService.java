package service;

import domain.*;
import domain.validators.exceptions.ValidationException;
import repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MessageService {
    public final Repository<Long, User> repoUser;
    public final Repository<Long, Message> repoMessage;
    public final Repository<Tuple<Long, Long>, Friendship> repoFriendship;

    /**
     * @param repoUser      User Repository
     * @param repoMessage   Message Repository
     */
    public MessageService(Repository<Long, User> repoUser,
                          Repository<Long, Message> repoMessage,
                          Repository<Tuple<Long, Long>, Friendship> repoFriendship) {
        this.repoUser = repoUser;
        this.repoMessage = repoMessage;
        this.repoFriendship = repoFriendship;
    }

    public void sendMessage(Long from, List<Long> to, String text) {
        if (to.contains(from)) {
            throw new ValidationException("Nu puteti sa va trimiteti singur mesaj");
        }

        if (text.length() == 0) {
            throw new ValidationException("Mesajul este gol!");
        }

        List<Long> friendsID = new ArrayList<>();
        repoFriendship.findAll().forEach(friendship -> {
            if (Objects.equals(friendship.getId().getLeft(), from)) {
                friendsID.add(friendship.getId().getRight());
            } else if (Objects.equals(friendship.getId().getRight(), from)) {
                friendsID.add(friendship.getId().getLeft());
            }
        });

        to.forEach(recipient -> {
            if (!friendsID.contains(recipient)) {
                throw new ValidationException("Nu puteti trimite mesaj unei persoane cu care nu sunteti prieteni");
            }

            Message msg = new Message(from, recipient, text, LocalDateTime.now());
            msg.setReply(0L);

            repoMessage.save(msg);
        });
    }

    public void sendReply(Long from, Long reply, String text) {
        Message msg = repoMessage.findOne(reply);

        if (text.length() == 0) {
            throw new ValidationException("Mesajul este gol!");
        }

        Message replied = new Message(from, msg.getFrom(), text, LocalDateTime.now());
        replied.setReply(msg.getId());

        repoMessage.save(replied);
    }

    public List<MessageDTO> getChat(Long currentUser, Long friend) {
        List<MessageDTO> messages = new ArrayList<>();
        repoMessage.findAll().forEach(message -> {
            if ((Objects.equals(message.getFrom(), currentUser) ||
                Objects.equals(message.getTo(), currentUser)) &&
                (Objects.equals(message.getTo(), friend) ||
                Objects.equals(message.getFrom(), friend))
            ) {
                MessageDTO msg = new MessageDTO(
                    repoUser.findOne(message.getFrom()),
                    repoUser.findOne(message.getTo()),
                    message.getText(),
                    message.getTimestamp()
                );
                msg.setId(message.getId());
                if (message.getReply() != 0) {
                    Message replyMsg = repoMessage.findOne(message.getReply());

                    if (replyMsg != null) {
                        MessageDTO replyMessage = new MessageDTO(
                            repoUser.findOne(replyMsg.getFrom()),
                            repoUser.findOne(replyMsg.getTo()),
                            replyMsg.getText(),
                            replyMsg.getTimestamp()
                        );
                        replyMessage.setId(replyMsg.getId());
                        msg.setReply(replyMessage);
                    }
                }
                messages.add(msg);
            }
        });
        return messages;
    }
}
