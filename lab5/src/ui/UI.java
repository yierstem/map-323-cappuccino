package ui;

import domain.Friendship;
import domain.Message;
import service.FriendRequestService;
import service.MessageService;
import service.Network;
import domain.User;
import domain.validators.exceptions.ValidationException;
import service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class UI {
    protected Service service;
    protected FriendRequestService frsrv;
    protected MessageService msgService;

    /**
     * constructor cu parametrii
     *
     * @param service de tip Service
     */
    public UI(Service service, FriendRequestService frsrv, MessageService msgService) {
        this.service = service;
        this.frsrv = frsrv;
        this.msgService = msgService;
    }

    private void showMenu() {
        System.out.println("+-----------------MENIU----------------+");
        System.out.println("| login. Autentificare utilizator      |");
        System.out.println("| 1.     Adauga utilizator             |");
        System.out.println("| 2.     Sterge utilizator             |");
        System.out.println("| 6.     Numar comunitati              |");
        System.out.println("| 7.     Cea mai sociabila comunitate  |");
        System.out.println("|       --------------------------     |");
        System.out.println("| a.     Afiseaza toti utilizatorii    |");
        System.out.println("| f.     Afiseaza toate prieteniile    |");
        System.out.println("| m.     Meniu                         |");
        System.out.println("| x.     Exit                          |");
        System.out.println("+--------------------------------------+");
    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        menu: while (true) {
            showMenu();
            try {
                System.out.print("Optiunea dvs: ");
                String option = scanner.nextLine();
                switch (option) {
                    case "login" -> loginUser();
                    case "1" -> ui_addUser();
                    case "2" -> ui_deleteUser();
                    case "6" -> ui_numberOfCommunities();
                    case "7" -> ui_biggestCommunity();
                    case "a" -> ui_printUsers();
                    case "f" -> ui_printFriendships();
                    case "x" -> { break menu; }
                    case "m" -> showMenu();
                    default -> System.out.println("Optiune invalida! Reincercati!");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    /**
     * 1.adauga un user
     */
    private void ui_addUser() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Adaugam utilizator!");

            System.out.print("Prenumele: ");
            String firstName = reader.readLine();

            System.out.print("Numele: ");
            String lastName = reader.readLine();

            service.addUser(firstName, lastName);
            System.out.println("Utilizatorul a fost adaugat cu succes!");

        } catch (IOException | ValidationException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 2. sterge un user
     */
    private void ui_deleteUser() {
        try {
            System.out.print("Dati id-ul userului de sters: ");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            Long id = Long.parseLong(reader.readLine());
            User removed = service.deleteUser(id);

            if (removed != null) {
                System.out.println("Userul sters este: " + removed);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 6 nr de comunitati (include si userii fara prietenii)
     */
    private void ui_numberOfCommunities() {
        Network network = getNetwork();
        System.out.println("Numarul de comunitati este: " + network.connectedComponents());
    }

    /**
     * 7.cea mai mare comunitate
     */
    private void ui_biggestCommunity() {
        Network network = getNetwork();
        System.out.println("Cea mai mare comunitate: ");
        network.biggestComponent().forEach(id -> System.out.println(service.getUserById(Long.valueOf(id))));
    }

    /**
     * a. afiseaza toti userii
     */
    private void ui_printUsers() {
        System.out.println("Toti utilizatorii: ");
        for (User u : service.getAllUsers()) {
            System.out.println(u);
        }
    }

    /**
     * f.  afiseaza toate prieteniile
     */
    private void ui_printFriendships() {
        System.out.println("Prietenii: ");
        for (Friendship f : service.getAllFriendships()) {
            System.out.println(f);
        }
    }

    /**
     * login. logare pt un userId
     */
    private void loginUser() {
        try {
            System.out.println("ID: ");
            Scanner scan = new Scanner(System.in);

            long loggedUserId = Long.parseLong(scan.next());
            User user = service.validateIdUser(loggedUserId);

            // transition to logged in menu
            Menu menu = new Menu(service, frsrv, msgService, user.getId());
            menu.runMenu();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * @return retea prepopulata cu informatii din baza de date
     */
    private Network getNetwork() {
        Network network = new Network(service.getSize());
        network.addUsers(service.getAllUsers());
        network.addFriendships(service.getAllFriendships());
        return network;
    }
}
