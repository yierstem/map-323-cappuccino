package ui;

import domain.*;
import domain.validators.exceptions.FriendshipException;
import domain.validators.exceptions.ValidationException;
import service.FriendRequestService;
import service.MessageService;
import service.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class Menu extends UI {
    private final long loggedUserId;

    public Menu(Service service, FriendRequestService frsrv, MessageService msgService, long loggedUserId) {
        super(service, frsrv, msgService);
        this.loggedUserId = loggedUserId;
    }

    private void showMenu() {
        System.out.println("+    -----------------MENIU--------------   +");
        System.out.println("   User ID: " + String.valueOf(loggedUserId) + " "
                + service.getUserById(loggedUserId).getFirstName() + " "
                + service.getUserById(loggedUserId).getLastName());
        System.out.println("| 1.      Modifica utilizator               |");
        System.out.println("| 2.      Afiseaza prietenii                |");
        System.out.println("| 3.      Afiseaza prietenii dupa luna      |");
        System.out.println("| 4.      Sterge prietenie                  |");
        System.out.println("| 5.      Trimite cerere prietenie          |");
        System.out.println("| 6.      Gestioneaza cereri de prietenie   |");
        System.out.println("| 7.      Trimite mesaj                     |");
        System.out.println("| 8.      Trimite ranspuns                  |");
        System.out.println("| 9.      Afiseaza mesaje                   |");
        System.out.println("|         -----------------------------     |");
        System.out.println("| a.      Afiseaza userii                   |");
        System.out.println("| f.      Afiseaza cererile trimise         |");
        System.out.println("| m.      Meniu                             |");
        System.out.println("| logout. Deautentificare                   |");
        System.out.println("+-------------------------------------------+");

    }

    public void runMenu() {
        Scanner scanner = new Scanner(System.in);
        menu: while (true) {
            showMenu();
            try {
                System.out.print("Optiunea dvs: ");
                String option = scanner.nextLine();
                switch (option) {
                    case "1" -> ui_updateUser();
                    case "2" -> ui_showFriends();
                    case "3" -> ui_showFriendsByMonth();
                    case "4" -> ui_deleteFriendship();
                    case "5" -> ui_sendFriendRequest();
                    case "6" -> ui_manageFriendRequest();
                    case "7" -> ui_sendMessage();
                    case "8" -> ui_sendReply();
                    case "9" -> ui_showChat();
                    case "a" -> ui_printUsers();
                    case "f" -> ui_printFriendRequests();
                    case "logout" -> { break menu; }
                    case "m" -> showMenu();
                    default -> System.out.println("Optiune invalida! Reincercati!");
                }
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void ui_showChat() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Conversatia cu (ID): ");
            Long friendID = Long.parseLong(reader.readLine());

            System.out.println("\t\t\t\t--- Chat ---");
            List<MessageDTO> chat = msgService.getChat(loggedUserId, friendID);
            chat.sort(Comparator.comparing(MessageDTO::getTimestamp));
            chat.forEach(message -> {
                if (message.getReply() != null) {
                    System.out.println("------------------------------------");
                    System.out.println(MessageFormat.format(
                        "######### [Reply to] {0}: {1}",
                        message.getReply().getFrom().getFirstName(),
                        message.getReply().getMessage()
                    ));
                }

                System.out.println(MessageFormat.format(
            "<ID:{0}> [{1}] {2}: {3}",
                    message.getId(),
                    message.getTimestamp().format(DateTimeFormatter.ofPattern("dd-M-yyyy hh:mm:ss")),
                    message.getFrom().getFirstName(),
                    message.getMessage()
                ));

                if (message.getReply() != null) {
                    System.out.println("------------------------------------");
                }
            });
            System.out.println("\t\t--- Sfarsit de conversatie ---");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void ui_sendMessage() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID-urile celor care vor primi mesajul (e.g. 1, 2, 3): ");
            String recipientsInput = reader.readLine();

            List<Long> recipients = new ArrayList<>();
            Arrays.stream(recipientsInput.split(",(\s?)"))
                    .forEach(id -> recipients.add(Long.parseLong(id)));

            System.out.print("Mesaj: ");
            String message = reader.readLine();

            msgService.sendMessage(loggedUserId, recipients, message);
            System.out.println("Mesajul a fost trimis!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void ui_sendReply() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Reply pentru mesajul cu ID: ");
            Long recipient = Long.parseLong(reader.readLine());

            System.out.print("Mesaj: ");
            String message = reader.readLine();

            msgService.sendReply(loggedUserId, recipient, message);
            System.out.println("Mesajul a fost trimis!");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 1. modifica un user
     */
    private void ui_updateUser() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

            System.out.print("Dati noul prenume: ");
            String firstName = reader.readLine();

            System.out.print("Dati noul nume: ");
            String lastName = reader.readLine();

            User updatedUser = service.updateUser(loggedUserId, firstName, lastName);
            System.out.println("Userul a fost modificat cu succes!");
            System.out.println(updatedUser);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    /**
     * 2. stream printeaza prietenii
     */
    private void ui_showFriends() {
        List<FriendshipDTO> friends = service.getFriendsStream(loggedUserId, service.getFriendsOfAUser(loggedUserId));
        friends.forEach(System.out::println);
        System.out.println();
    }

    /**
     * 3. stream printeaza prietenii dupa luna
     */
    private void ui_showFriendsByMonth() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Luna: ");
            int month = Integer.parseInt(reader.readLine());
            service.validateData(month);

            List<FriendshipDTO> friends = service.getFriendsStream(
                loggedUserId,
                service.getFriendsOfAUser(loggedUserId, month)
            );

            friends.forEach(System.out::println);
            System.out.println();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 5
     */
    private void ui_deleteFriendship() {
        System.out.println("Stergeti o prietenie");
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("ID: ");
            Long id2 = Long.parseLong(reader.readLine());
            Friendship removed = service.removeFriendship(loggedUserId, id2);

            if (removed != null) {
                User first_user = service.getUserById(removed.getId().getLeft());
                User second_user = service.getUserById(removed.getId().getRight());

                System.out.println(
                    MessageFormat.format(
                        "A fost stearsa prietenia dintre: {0} {1} si {2} {3}",
                        first_user.getFirstName(), first_user.getLastName(),
                        second_user.getFirstName(), second_user.getLastName()
                    )
                );
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 6. trimite o cerere de prietenie
     */
    private void ui_sendFriendRequest() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Cerere de prietenie catre utilizatorul cu ID: ");

            Long userRequested = Long.parseLong(reader.readLine());
            frsrv.sendFriendRequest(loggedUserId, userRequested);
            System.out.println("Cererea de prietenie a fost trimisa!");
        } catch (IOException | ValidationException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 7. gestioneaza cererile de prietenie
     */
    private void ui_manageFriendRequest() {
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            Iterable<FriendRequest> requests = frsrv.getFriendRequests(loggedUserId);

            if (frsrv.friendRequestsCount(loggedUserId) == 0) {
                System.out.println("Nu aveti cereri de prietenie");
                return;
            }

            System.out.println("Cererile dvs. sunt de la: ");
            requests.forEach(request -> {
                User u = frsrv.repoUser.findOne(request.getId().getLeft());
                System.out.println(
                    MessageFormat.format(
                "ID: {0} First Name: {1} Last Name: {2}\tStatus: {3}",
                        u.getId(), u.getFirstName(), u.getLastName(), request.getStatus()
                    )
                );
            });

            System.out.println("ID user: ");
            long friendId = Long.parseLong(reader.readLine());
            service.validateIdUser(friendId);

            System.out.println("Aceptati cererea? y/n:");
            String choice = reader.readLine();

            switch (choice) {
                case "y" -> frsrv.manageFriendRequest(friendId, loggedUserId, Status.APPROVED);
                case "n" -> frsrv.manageFriendRequest(friendId, loggedUserId, Status.REJECTED);
                default -> System.out.println("Optiune invalida!");
            }
        } catch (IOException | ValidationException | IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }

    /**
     * 8. afiseaza toti userii
     */
    private void ui_printUsers() {
        service.getAllUsers().forEach(System.out::println);
        System.out.println();
    }
    /**
     *f.  afiseaza toate cererile de prietenie trimise
     */
    private void ui_printFriendRequests() {
        System.out.println("Cererile de prietenie care inca sunt PENDING:\n");
        frsrv.getSentFriendRequests(loggedUserId).forEach(System.out::println);
    }
}
