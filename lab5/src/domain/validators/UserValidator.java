package domain.validators;

import domain.User;
import domain.validators.exceptions.ValidationException;

public class UserValidator implements Validator<User> {
    public UserValidator(){

    }
    @Override
    public void validate(User entity) throws ValidationException {
        //to do
        String erros = "";
        if(entity.getFirstName().equals("")){
            erros += "First name must not be null\n";
        }
        if(entity.getLastName().equals("")){
            erros += "Last name must not be null\n";
        }
        if(!erros.equals("")){
            throw new ValidationException(erros);
        }
    }
}
