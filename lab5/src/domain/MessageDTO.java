package domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class MessageDTO extends Entity<Long> {
    private User from;
    private User to;
    private String message;
    private LocalDateTime timestamp;
    private MessageDTO reply;

    public MessageDTO() {}

    public MessageDTO(User from, User to, String message, LocalDateTime timestamp) {
        this.from = from;
        this.to = to;
        this.message = message;
        this.timestamp = timestamp;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public MessageDTO getReply() {
        return reply;
    }

    public void setReply(MessageDTO reply) {
        this.reply = reply;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MessageDTO)) return false;
        if (!super.equals(o)) return false;
        MessageDTO message = (MessageDTO) o;
        return Objects.equals(getFrom(), message.getFrom()) && Objects.equals(getTo(), message.getTo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFrom(), getTo());
    }

    @Override
    public String toString() {
        return "Message{" +
                "from=" + from +
                ", to=" + to +
                '}';
    }
}
