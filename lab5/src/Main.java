import domain.*;
import domain.validators.FriendRequestValidator;
import domain.validators.MessageValidator;
import repository.Repository;
import repository.db.FriendRequestDbRepository;
import repository.db.FriendshipDbRepository;
import repository.db.MessageDbRepository;
import repository.db.UserDbRepository;
import repository.file.FriendRequestFileRepository;
import repository.file.FriendshipFileRepository;
import repository.file.UserFileRepository;
import service.FriendRequestService;
import service.MessageService;
import service.Service;

import ui.UI;
import domain.validators.FriendshipValidator;
import domain.validators.UserValidator;


public class Main {
    public static void main(String[] args) {
        UserValidator userValidator = new UserValidator();
        MessageValidator msgValidator = new MessageValidator();
        FriendshipValidator friendshipValidator = new FriendshipValidator();
        FriendRequestValidator friendRequestValidator = new FriendRequestValidator();

        //Repository<Long, User> repoUser = new UserFileRepository("data/users.csv", userValidator);
        //Repository<Tuple<Long, Long>, Friendship> repoFriendship = new FriendshipFileRepository("data/friendship.csv", friendshipValidator);
        //Repository<Tuple<Long, Long>, FriendRequest> repoFriendRequest = new FriendRequestFileRepository("data/friendRequest.csv", friendRequestValidator);
        //InMemoryRepository<Long, User> repoUser = new InMemoryRepository<>(userValidator);
        //InMemoryRepository<Tuple<Long, Long>, Friendship> repoFriendship = new InMemoryRepository<>(friendshipValidator);

        //jdbc:postgresql://localhost:5432/SocialNetwork

        Repository<Long, User> repoDbUser = new UserDbRepository(
            "jdbc:postgresql://localhost:5432/SocialNetwork",
            "postgres",
            System.getenv("PASSWORD"),
            userValidator
        );
        Repository<Tuple<Long, Long>, Friendship> repoDbFriendship = new FriendshipDbRepository(
            "jdbc:postgresql://localhost:5432/SocialNetwork",
            "postgres",
            System.getenv("PASSWORD"),
            friendshipValidator
        );
        Repository<Tuple<Long, Long>, FriendRequest> repoDbFriendRequest = new FriendRequestDbRepository(
            "jdbc:postgresql://localhost:5432/SocialNetwork",
            "postgres",
            System.getenv("PASSWORD"),
            friendRequestValidator
        );
        Repository<Long, Message> repoDbMsg = new MessageDbRepository(
                "jdbc:postgresql://localhost:5432/SocialNetwork",
                "postgres",
                System.getenv("PASSWORD"),
                msgValidator
        );

        Service service = new Service(repoDbUser, repoDbFriendship);
        FriendRequestService frsrv = new FriendRequestService(repoDbUser, repoDbFriendship, repoDbFriendRequest);
        MessageService msgService = new MessageService(repoDbUser, repoDbMsg, repoDbFriendship);

        UI ui = new UI(service, frsrv, msgService);
        ui.runMenu();
    }
}
